﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ConsoleApp1
{

    class Program
    {
        static void Main(string[] args)
        {
            var student1 = new Student
            {
                Id = 1,
                Name = "John",
                SurName = "Smith"
            };
            var serialized = JsonConvert.SerializeObject(student1);
            Console.WriteLine(serialized);
          
            File.WriteAllText("Desktop:\\s.txt", serialized);
            //var text = File.ReadAllText("D:\\s.txt");
            //var student2 = JsonConvert.DeserializeObject<Student>(text);
        }
    }
}

